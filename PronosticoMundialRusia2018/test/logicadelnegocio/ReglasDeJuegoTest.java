/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logicadelnegocio;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author speralta
 */
public class ReglasDeJuegoTest {
    public static ReglasDeJuego instance = 
            ReglasDeJuego.obtenerInstanciaReglasDeJuego();
    
    public ReglasDeJuegoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        instance.setAciertoEmpateFaseFinal(10);
        instance.setAciertoEmpateFaseGrupo(10);
        instance.setAciertoGanadorFaseFinal(10);
        instance.setAciertoGanadorFaseGrupo(10);
        instance.setResultadoExactoFaseFinal(10);
        instance.setResultadoExactoFaseGrupo(10);
    }
    
    @After
    public void tearDown() {
        instance.setAciertoEmpateFaseFinal(10);
        instance.setAciertoEmpateFaseGrupo(10);
        instance.setAciertoGanadorFaseFinal(10);
        instance.setAciertoGanadorFaseGrupo(10);
        instance.setResultadoExactoFaseFinal(10);
        instance.setResultadoExactoFaseGrupo(10);
    }

    /**
     * Test of setAciertoGanadorFaseGrupo method, of class ReglasDeJuego.
     */
    @Test
    public void testSetAciertoGanadorFaseGrupo() {
        System.out.println("setAciertoGanadorFaseGrupo");
        int aciertoGanadorFaseGrupo = 10;
        instance.setAciertoGanadorFaseGrupo(aciertoGanadorFaseGrupo);
        assertEquals((Object) aciertoGanadorFaseGrupo ,
                instance.getAciertoGanadorFaseGrupo());
    }

    /**
     * Test of setAciertoGanadorFaseFinal method, of class ReglasDeJuego.
     */
    @Test
    public void testSetAciertoGanadorFaseFinal() {
        System.out.println("setAciertoGanadorFaseFinal");
        int aciertoGanadorFaseFinal = 19;
        instance.setAciertoGanadorFaseFinal(aciertoGanadorFaseFinal);
        assertEquals((Object) aciertoGanadorFaseFinal ,
                instance.getAciertoGanadorFaseFinal());
    }

    /**
     * Test of setAciertoEmpateFaseGrupo method, of class ReglasDeJuego.
     */
    @Test
    public void testSetAciertoEmpateFaseGrupo() {
        System.out.println("setAciertoEmpateFaseGrupo");
        int aciertoEmpateFaseGrupo = 26;
        instance.setAciertoEmpateFaseGrupo(aciertoEmpateFaseGrupo);
        assertEquals((Object) aciertoEmpateFaseGrupo ,
                instance.getAciertoEmpateFaseGrupo());
    }

    /**
     * Test of setAciertoEmpateFaseFinal method, of class ReglasDeJuego.
     */
    @Test
    public void testSetAciertoEmpateFaseFinal() {
        System.out.println("setAciertoEmpateFaseFinal");
        int aciertoEmpateFaseFinal = 15;
        instance.setAciertoEmpateFaseFinal(aciertoEmpateFaseFinal);
        assertEquals((Object)aciertoEmpateFaseFinal ,
                instance.getAciertoEmpateFaseFinal());
    }

    /**
     * Test of setResultadoExactoFaseGrupo method, of class ReglasDeJuego.
     */
    @Test
    public void testSetResultadoExactoFaseGrupo() {
        System.out.println("setResultadoExactoFaseGrupo");
        int resultadoExactoFaseGrupo = 11;
        instance.setResultadoExactoFaseGrupo(resultadoExactoFaseGrupo);
        assertEquals((Object) resultadoExactoFaseGrupo ,
                instance.getResultadoExactoFaseGrupo());
    }

    /**
     * Test of setResultadoExactoFaseFinal method, of class ReglasDeJuego.
     */
    @Test
    public void testSetResultadoExactoFaseFinal() {
        System.out.println("setResultadoExactoFaseFinal");
        int resultadoExactoFaseFinal = 12;
        instance.setResultadoExactoFaseFinal(resultadoExactoFaseFinal);
        assertEquals((Object) resultadoExactoFaseFinal ,
                instance.getResultadoExactoFaseFinal());
    }

    /**
     * Test of getAciertoGanadorFaseGrupo method, of class ReglasDeJuego.
     */
    @Test
    public void testGetAciertoGanadorFaseGrupo() {
        System.out.println("getAciertoGanadorFaseGrupo");
        int expResult = 10;
        int result = instance.getAciertoGanadorFaseGrupo();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAciertoGanadorFaseFinal method, of class ReglasDeJuego.
     */
    @Test
    public void testGetAciertoGanadorFaseFinal() {
        System.out.println("getAciertoGanadorFaseFinal");
        int expResult = 10;
        int result = instance.getAciertoGanadorFaseFinal();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAciertoEmpateFaseGrupo method, of class ReglasDeJuego.
     */
    @Test
    public void testGetAciertoEmpateFaseGrupo() {
        System.out.println("getAciertoEmpateFaseGrupo");
        int expResult = 10;
        int result = instance.getAciertoEmpateFaseGrupo();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAciertoEmpateFaseFinal method, of class ReglasDeJuego.
     */
    @Test
    public void testGetAciertoEmpateFaseFinal() {
        System.out.println("getAciertoEmpateFaseFinal");
        int expResult = 10;
        int result = instance.getAciertoEmpateFaseFinal();
        assertEquals(expResult, result);
    }

    /**
     * Test of getResultadoExactoFaseGrupo method, of class ReglasDeJuego.
     */
    @Test
    public void testGetResultadoExactoFaseGrupo() {
        System.out.println("getResultadoExactoFaseGrupo");
        int expResult = 10;
        int result = instance.getResultadoExactoFaseGrupo();
        assertEquals(expResult, result);
    }

    /**
     * Test of getResultadoExactoFaseFinal method, of class ReglasDeJuego.
     */
    @Test
    public void testGetResultadoExactoFaseFinal() {
        System.out.println("getResultadoExactoFaseFinal");
        int expResult = 10;
        int result = instance.getResultadoExactoFaseFinal();
        assertEquals(expResult, result);
    }
}
