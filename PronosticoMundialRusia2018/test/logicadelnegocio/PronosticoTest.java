/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logicadelnegocio;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author speralta
 */
public class PronosticoTest {
    ReglasDeJuego reglasDeJuego;
    Partido partido;
    Participante participante;
    Pronostico pronostico;
    
    public PronosticoTest() {
        
    }
    
    @BeforeClass
    public static void setUpClass() {
       
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        reglasDeJuego = ReglasDeJuego.obtenerInstanciaReglasDeJuego();
        
        reglasDeJuego.setAciertoEmpateFaseGrupo(2);        
        reglasDeJuego.setAciertoGanadorFaseGrupo(2);
        reglasDeJuego.setResultadoExactoFaseGrupo(4);
        
        reglasDeJuego.setAciertoEmpateFaseFinal(3);
        reglasDeJuego.setAciertoGanadorFaseFinal(3);
        reglasDeJuego.setResultadoExactoFaseFinal(5);
        
        
        partido = new Partido();
        partido.setFase("GRUPO");
        partido.setGolesPais1(2);
        partido.setGolesPais2(2);
        
        participante = new Participante("Jugador1");
        
        pronostico = new Pronostico(partido);         
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setPronosticoPais1 method, of class Pronostico.
     */
    @Test
    public void testSetPronosticoPais1() {
        System.out.println("setPronosticoPais1");
        int pronosticoPais1 = 2;        
        pronostico.setPronosticoPais1(pronosticoPais1);
        int resultadoEsperado = 2;
        assertEquals(resultadoEsperado, pronostico.getPronosticoPais1(), 0);
    }

    /**
     * Test of setPronosticoPais2 method, of class Pronostico.
     */
    @Test
    public void testSetPronosticoPais2() {
        System.out.println("setPronosticoPais2");
        int pronosticoPais2 = 3;        
        pronostico.setPronosticoPais2(pronosticoPais2);
        int resultadoEsperado = 3;
        assertEquals(resultadoEsperado, pronostico.getPronosticoPais2(), 0);
    }

    /**
     * Test of getPartido method, of class Pronostico.
     */
    @Test
    public void testGetPartido() {
        Partido p = pronostico.getPartido();
        Object[] o1 = {"GRUPO", "2", "2"};        
        Object[] o2 = {p.getFase(), p.getGolesPais1(), p.getGolesPais2()};
        assertArrayEquals(o2, o2);       
    }
    
    /**
     * Test of setPuntaje method, of class Pronostico.
     */
    @Test
    public void testSetPuntajeFaseGrupoEmpate() {
        System.out.println("setPuntaje");
        reglasDeJuego.setAciertoEmpateFaseGrupo(2);
        reglasDeJuego.setAciertoGanadorFaseGrupo(2);
        reglasDeJuego.setResultadoExactoFaseGrupo(4);
        reglasDeJuego.setAciertoEmpateFaseFinal(3);
        reglasDeJuego.setAciertoGanadorFaseFinal(3);
        reglasDeJuego.setResultadoExactoFaseFinal(5);
        partido.setFase("GRUPO");
        partido.setGolesPais1(2);
        partido.setGolesPais2(2);

        pronostico = new Pronostico(partido);

        pronostico.setPronosticoPais1(1);
        pronostico.setPronosticoPais2(1);

        int resultadoEsperado = 2;
        assertEquals(resultadoEsperado, pronostico.getPuntaje(), 0);
    }

    @Test
    public void testSetPuntajeFaseGrupoGanador() {
        System.out.println("setPuntaje");
        reglasDeJuego.setAciertoEmpateFaseGrupo(2);
        reglasDeJuego.setAciertoGanadorFaseGrupo(2);
        reglasDeJuego.setResultadoExactoFaseGrupo(4);

        reglasDeJuego.setAciertoEmpateFaseFinal(3);
        reglasDeJuego.setAciertoGanadorFaseFinal(3);
        reglasDeJuego.setResultadoExactoFaseFinal(5);

        partido.setFase("GRUPO");
        partido.setGolesPais1(2);
        partido.setGolesPais2(3);

        pronostico = new Pronostico(partido);

        pronostico.setPronosticoPais1(1);
        pronostico.setPronosticoPais2(2);

        int resultadoEsperado = 2;
        assertEquals(resultadoEsperado, pronostico.getPuntaje(), 0);
    }
    
    @Test
    public void testSetPuntajeFaseGrupoGanador2() {
        System.out.println("setPuntaje");
        reglasDeJuego.setAciertoEmpateFaseGrupo(2);
        reglasDeJuego.setAciertoGanadorFaseGrupo(2);
        reglasDeJuego.setResultadoExactoFaseGrupo(4);

        reglasDeJuego.setAciertoEmpateFaseFinal(3);
        reglasDeJuego.setAciertoGanadorFaseFinal(3);
        reglasDeJuego.setResultadoExactoFaseFinal(5);

        partido.setFase("GRUPO");
        partido.setGolesPais1(3);
        partido.setGolesPais2(2);

        pronostico = new Pronostico(partido);

        pronostico.setPronosticoPais1(2);
        pronostico.setPronosticoPais2(1);

        int resultadoEsperado = 2;
        assertEquals(resultadoEsperado, pronostico.getPuntaje(), 0);
    }

    @Test
    public void testSetPuntajeFaseGrupoExacto() {
        System.out.println("setPuntaje");
        reglasDeJuego.setAciertoEmpateFaseGrupo(2);        
        reglasDeJuego.setAciertoGanadorFaseGrupo(2);
        reglasDeJuego.setResultadoExactoFaseGrupo(4);

        reglasDeJuego.setAciertoEmpateFaseFinal(3);
        reglasDeJuego.setAciertoGanadorFaseFinal(3);
        reglasDeJuego.setResultadoExactoFaseFinal(5);
        
        partido.setFase("GRUPO");
        partido.setGolesPais1(2);
        partido.setGolesPais2(3);

        pronostico = new Pronostico(partido);

        pronostico.setPronosticoPais1(2);
        pronostico.setPronosticoPais2(3);

        int resultadoEsperado = 4;
        assertEquals(resultadoEsperado, pronostico.getPuntaje(), 0);
    }
    
    
    @Test
    public void testSetPuntajeFaseFinalEmpate() {
        System.out.println("setPuntaje");
        reglasDeJuego.setAciertoEmpateFaseGrupo(2);        
        reglasDeJuego.setAciertoGanadorFaseGrupo(2);
        reglasDeJuego.setResultadoExactoFaseGrupo(4);
        
        reglasDeJuego.setAciertoEmpateFaseFinal(3);
        reglasDeJuego.setAciertoGanadorFaseFinal(3);
        reglasDeJuego.setResultadoExactoFaseFinal(5);
        
        partido.setFase("FINAL");
        partido.setGolesPais1(2);
        partido.setGolesPais2(2);
       
        pronostico = new Pronostico(partido);
        pronostico.setPronosticoPais1(1);
        pronostico.setPronosticoPais2(1);
        
        int resultadoEsperado = 3;
        assertEquals(resultadoEsperado, pronostico.getPuntaje(), 0);
    }

    @Test
    public void testSetPuntajeFaseFinalGanador() {
        System.out.println("setPuntaje");
        reglasDeJuego.setAciertoEmpateFaseGrupo(2);        
        reglasDeJuego.setAciertoGanadorFaseGrupo(2);
        reglasDeJuego.setResultadoExactoFaseGrupo(4);
        
        reglasDeJuego.setAciertoEmpateFaseFinal(3);
        reglasDeJuego.setAciertoGanadorFaseFinal(3);
        reglasDeJuego.setResultadoExactoFaseFinal(5);
        
        partido.setFase("FINAL");
        partido.setGolesPais1(2);
        partido.setGolesPais2(3);
        
        pronostico = new Pronostico(partido);        
        pronostico.setPronosticoPais1(1);
        pronostico.setPronosticoPais2(2);
        
        int resultadoEsperado = 3;
        assertEquals(resultadoEsperado, pronostico.getPuntaje(), 0);
    }
    
    @Test
    public void testSetPuntajeFaseFinalExacto() {
        System.out.println("setPuntaje");
        reglasDeJuego.setAciertoEmpateFaseGrupo(2);        
        reglasDeJuego.setAciertoGanadorFaseGrupo(2);
        reglasDeJuego.setResultadoExactoFaseGrupo(4);
        
        reglasDeJuego.setAciertoEmpateFaseFinal(3);
        reglasDeJuego.setAciertoGanadorFaseFinal(3);
        reglasDeJuego.setResultadoExactoFaseFinal(5);
        
        partido.setFase("FINAL");
        partido.setGolesPais1(2);
        partido.setGolesPais2(3);
        
        pronostico = new Pronostico(partido);        
        pronostico.setPronosticoPais1(2);
        pronostico.setPronosticoPais2(3);
        
        int resultadoEsperado = 5;
        assertEquals(resultadoEsperado, pronostico.getPuntaje(), 0);
    }
    
    @Test
    public void testSetPuntajeSinAcierto() {
        System.out.println("setPuntaje");
        reglasDeJuego.setAciertoEmpateFaseGrupo(2);        
        reglasDeJuego.setAciertoGanadorFaseGrupo(2);
        reglasDeJuego.setResultadoExactoFaseGrupo(4);
        
        reglasDeJuego.setAciertoEmpateFaseFinal(3);
        reglasDeJuego.setAciertoGanadorFaseFinal(3);
        reglasDeJuego.setResultadoExactoFaseFinal(5);
        
        partido.setFase("FINAL");
        partido.setGolesPais1(2);
        partido.setGolesPais2(3);
        
        pronostico = new Pronostico(partido);        
        pronostico.setPronosticoPais1(3);
        pronostico.setPronosticoPais2(2);
        
        int resultadoEsperado = 0;
        assertEquals(resultadoEsperado, pronostico.getPuntaje(), 0);
    }
    
    /**
     * Test of getPronosticoPais1 method, of class Pronostico.
     */
    @Test
    public void testGetPronosticoPais1() {
        System.out.println("getPronosticoPais1");
        pronostico.setPronosticoPais1(2);
        int expResult = 2;
        int result = pronostico.getPronosticoPais1();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPronosticoPais2 method, of class Pronostico.
     */
    @Test
    public void testGetPronosticoPais2() {
        System.out.println("getPronosticoPais2");
        pronostico.setPronosticoPais2(3);
        int expResult = 3;
        int result = pronostico.getPronosticoPais2();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPuntaje method, of class Pronostico.
     */
    @Test
    public void testGetPuntaje() {
        System.out.println("getPuntaje");
        
        reglasDeJuego.setAciertoEmpateFaseGrupo(2);        
        reglasDeJuego.setAciertoGanadorFaseGrupo(2);
        reglasDeJuego.setResultadoExactoFaseGrupo(4);
        
        reglasDeJuego.setAciertoEmpateFaseFinal(3);
        reglasDeJuego.setAciertoGanadorFaseFinal(3);
        reglasDeJuego.setResultadoExactoFaseFinal(5);
        
        partido.setFase("GRUPO");
        partido.setGolesPais1(2);
        partido.setGolesPais2(2);
        
        pronostico = new Pronostico(partido);
        pronostico.setPronosticoPais1(2);
        pronostico.setPronosticoPais2(2);
        
        int expResult = 4;
        int result = pronostico.getPuntaje();
        assertEquals(expResult, result);
    }
    
}
