/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logicadelnegocio;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author speralta
 */
public class PartidoTest {
    
    public PartidoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setGrupo method, of class Partido.
     */
    @Test
    public void testSetGrupo() {
        System.out.println("setGrupo");
        String grupo = "GRUPOA";
        Partido instance = new Partido();
        instance.setGrupo(grupo);
        assertEquals(grupo, instance.getGrupo());
    }

    /**
     * Test of setFase method, of class Partido.
     */
    @Test
    public void testSetFase() {
        System.out.println("setFase");
        String fase = "FASE";
        Partido instance = new Partido();
        instance.setFase(fase);
        assertEquals(fase, instance.getFase());
    }

    /**
     * Test of setPais1 method, of class Partido.
     */
    @Test
    public void testSetPais1() {
        System.out.println("setPais1");
        String pais1 = "PAIS1";
        Partido instance = new Partido();
        instance.setPais1(pais1);
        assertEquals(pais1, instance.getPais1());
    }

    /**
     * Test of setPais2 method, of class Partido.
     */
    @Test
    public void testSetPais2() {
        System.out.println("setPais2");
        String pais2 = "PAIS2";
        Partido instance = new Partido();
        instance.setPais2(pais2);
        assertEquals(pais2, instance.getPais2());
    }

    /**
     * Test of setGolesPais1 method, of class Partido.
     */
    @Test
    public void testSetGolesPais1() {
        System.out.println("setGolesPais1");
        int golesPais1 = 10;
        Partido instance = new Partido();
        instance.setGolesPais1(golesPais1);
        assertEquals((Object) golesPais1, instance.getGolesPais1());
    }

    /**
     * Test of setGolesPais2 method, of class Partido.
     */
    @Test
    public void testSetGolesPais2() {
        System.out.println("setGolesPais2");
        int golesPais2 = 10;
        Partido instance = new Partido();
        instance.setGolesPais2(golesPais2);
        assertEquals((Object) golesPais2, instance.getGolesPais2());
    }

    /**
     * Test of setLugar method, of class Partido.
     */
    @Test
    public void testSetLugar() {
        System.out.println("setLugar");
        String lugar = "En algun lugar de la Mancha";
        Partido instance = new Partido();
        instance.setLugar(lugar);
        assertEquals(lugar, instance.getLugar());
    }

    /**
     * Test of setFechaHora method, of class Partido.
     */
    @Test
    public void testSetFechaHora() throws ParseException {
        System.out.println("setFechaHora");
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date fechaHora = format.parse("12/06/2018 09:00");
        Partido instance = new Partido();
        instance.setFechaHora(fechaHora);
        assertEquals(fechaHora, instance.getFechaHora());
    }

    /**
     * Test of getGrupo method, of class Partido.
     */
    @Test
    public void testGetGrupo() {
        System.out.println("getGrupo");
        String grupo = "GRUPOA";
        Partido instance = new Partido();
        instance.setGrupo(grupo);
        assertEquals(grupo, instance.getGrupo());
    }

    /**
     * Test of getFase method, of class Partido.
     */
    @Test
    public void testGetFase() {
        System.out.println("getFase");
        String fase = "FASE";
        Partido instance = new Partido();
        instance.setFase(fase);
        assertEquals(fase, instance.getFase());
    }

    /**
     * Test of getPais1 method, of class Partido.
     */
    @Test
    public void testGetPais1() {
        System.out.println("getPais1");
        String pais1 = "PAIS1";
        Partido instance = new Partido();
        instance.setPais1(pais1);
        assertEquals(pais1, instance.getPais1());
    }

    /**
     * Test of getPais2 method, of class Partido.
     */
    @Test
    public void testGetPais2() {
        System.out.println("getPais2");
        String pais2 = "PAIS2";
        Partido instance = new Partido();
        instance.setPais2(pais2);
        assertEquals(pais2, instance.getPais2());
    }

    /**
     * Test of getGolesPais1 method, of class Partido.
     */
    @Test
    public void testGetGolesPais1() {
        System.out.println("getGolesPais1");
        int golesPais1 = 10;
        Partido instance = new Partido();
        instance.setGolesPais1(golesPais1);
        assertEquals((Object) golesPais1, instance.getGolesPais1());
    }

    /**
     * Test of getGolesPais2 method, of class Partido.
     */
    @Test
    public void testGetGolesPais2() {
        System.out.println("getGolesPais2");
        int golesPais2 = 10;
        Partido instance = new Partido();
        instance.setGolesPais2(golesPais2);
        assertEquals((Object) golesPais2, instance.getGolesPais2());
    }

    /**
     * Test of getLugar method, of class Partido.
     */
    @Test
    public void testGetLugar() {
        System.out.println("getLugar");
        String lugar = "En algun lugar de la Mancha";
        Partido instance = new Partido();
        instance.setLugar(lugar);
        assertEquals(lugar, instance.getLugar());
    }

    /**
     * Test of getFechaHora method, of class Partido.
     */
    @Test
    public void testGetFechaHora() throws ParseException {
        System.out.println("getFechaHora");
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date fechaHora = format.parse("12/06/2018 09:00");
        Partido instance = new Partido();
        instance.setFechaHora(fechaHora);
        assertEquals(fechaHora, instance.getFechaHora());
    }
    
    @Test
    public void testEquals() throws ParseException {
        System.out.println("testEquals");
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date fecha = format.parse("14/06/2018 12:00");
        Partido partido1 = new Partido("Grupo", "fase", "pais1", "pais2",
                0, 0, "lugar", fecha);
        Partido partido2 = new Partido("Grupo", "fase", "pais1", "pais2",
                0, 0, "lugar", fecha);

        assertEquals(true, partido1.equals(partido2));
    }
    
    @Test
    public void testNotEquals() throws ParseException {
        System.out.println("testNotEquals");
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date fecha = format.parse("14/06/2018 12:00");
        Partido partido1 = new Partido("Grupo1", "fase2", "pais1", "pais2",
                5, 3, "lugar2", fecha);
        Partido partido2 = new Partido("Grupo2", "fase1", "pais1", "pais2",
                2, 1, "lugar1", fecha);

        assertEquals(false, partido1.equals(partido2));
    }
}
