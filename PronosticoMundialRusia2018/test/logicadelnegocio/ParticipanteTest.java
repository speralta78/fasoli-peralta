/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logicadelnegocio;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author speralta
 */
public class ParticipanteTest {
    Participante participante1;
    Participante participante2;
    ArrayList<Pronostico> pronosticos = new ArrayList<>();
    public static final int GOLES_1= 5;
    public static final int GOLES_2= 0;
    public static final int GOLES_3= 3;
    public static final int GOLES_4= 2;
    
    public static final int GOLES_PRON_1= 3;
    public static final int GOLES_PRON_2= 1;
    public static final int GOLES_PRON_3= 2;
    public static final int GOLES_PRON_4= 2;
    
    public static final int PUNTAJE_TOTAL_1 = 10;
    public static final int PUNTAJE_TOTAL_2 = 12;
    
    public ParticipanteTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws ParseException {
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date fecha1 = format.parse("24/06/2018 15:30");
        Date fecha2 = format.parse("06/07/2018 12:00");
        Partido partido1 = new Partido("Grupo A", "GRUPO", "Rusia",
                "Arabia Saudita", GOLES_1, GOLES_2, "Estadio 1", fecha1);
        Partido partido2 = new Partido("Grupo H", "OCTAVOS", "Alemania",
                "España", GOLES_3, GOLES_4, "Estadio 2", fecha2);

        Pronostico pronostico1 = new Pronostico(partido1);
        pronostico1.setPronosticoPais1(GOLES_PRON_1);
        pronostico1.setPronosticoPais2(GOLES_PRON_2);

        Pronostico pronostico2 = new Pronostico(partido2);
        pronostico2.setPronosticoPais1(GOLES_PRON_3);
        pronostico2.setPronosticoPais2(GOLES_PRON_4);

        pronosticos.add(pronostico1);
        pronosticos.add(pronostico2);

        participante1 = new Participante("Pedro");
        participante1.setPronosticos(pronosticos);
        participante1.setPuntajeTotal(PUNTAJE_TOTAL_1);
        
        participante2 = new Participante("Luis");
        participante2.setPronosticos(pronosticos);
        participante2.setPuntajeTotal(PUNTAJE_TOTAL_2);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of setAlias method, of class Participante.
     */
    @Test
    public void testSetAlias() {
        System.out.println("setAlias");

        String nuevoAlias = "Lucas";
        participante1.setAlias(nuevoAlias);

        assertEquals(nuevoAlias, participante1.getAlias());
    }

    /**
     * Test of getAlias method, of class Participante.
     */
    @Test
    public void testGetAlias() {
        System.out.println("getAlias");
        String alias = "Pedro";
        
        assertEquals(alias, participante1.getAlias());
    }

    /**
     * Test of setPuntajeTotal and getPuntajeTotal method, of class Participante.
     */
    @Test
    public void testGetSetPuntajeTotal() {
        System.out.println("setPuntajeTotal");
        Object puntajeTotal = PUNTAJE_TOTAL_1;
        participante1.setPuntajeTotal(Integer.valueOf(puntajeTotal.toString()));

        assertEquals(puntajeTotal, participante1.getPuntajeTotal());
    }

    /**
     * Test of setPronosticos method, of class Participante.
     */
    @Test
    public void testSetPronosticos() throws ParseException {
        System.out.println("setPronosticos");

        DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date fecha1 = format.parse("24/06/2018 09:30");
        Date fecha2 = format.parse("06/07/2018 11:00");
        Partido partido1 = new Partido("Grupo B", "OCTAVOS", "Brasil",
                "Uruguay", GOLES_4, GOLES_3, "Estadio 3", fecha1);
        Partido partido2 = new Partido("Grupo H", "GRUPOS", "Egipto",
                "Portugal", GOLES_2, GOLES_1, "Estadio 4", fecha2);

        Pronostico pronostico1 = new Pronostico(partido1);
        pronostico1.setPronosticoPais1(GOLES_PRON_4);
        pronostico1.setPronosticoPais2(GOLES_PRON_3);

        Pronostico pronostico2 = new Pronostico(partido2);
        pronostico2.setPronosticoPais1(GOLES_PRON_2);
        pronostico2.setPronosticoPais2(GOLES_PRON_1);

        ArrayList<Pronostico> pronosticosLocales = new ArrayList<>();

        pronosticosLocales.add(pronostico1);
        pronosticosLocales.add(pronostico2);

        participante1.setPronosticos(pronosticosLocales);

        Object[] o1 = {pronosticosLocales.get(0).getPartido(),
                       pronosticosLocales.get(1).getPartido(),
                       pronosticosLocales.get(0).getPronosticoPais1(),
                       pronosticosLocales.get(0).getPronosticoPais2(),
                       pronosticosLocales.get(1).getPronosticoPais1(),
                       pronosticosLocales.get(1).getPronosticoPais2()};
        Object[] o2 = {participante1.getPronosticos().get(0).getPartido(),
                       participante1.getPronosticos().get(1).getPartido(),
                       GOLES_PRON_4,
                       GOLES_PRON_3,
                       GOLES_PRON_2,
                       GOLES_PRON_1};

        assertArrayEquals(o2, o2);

    }
    
    @After
    public void seteoPronosticos(){
        participante1.setPronosticos(pronosticos);
    }

    /**
     * Test of getPronosticos method, of class Participante.
     */
    @Test
    public void testGetPronosticos() {
        System.out.println("getPronosticos");

        ArrayList<Pronostico> pronosObtenidos = participante1.getPronosticos();

        Object[] o1 = {pronosObtenidos.get(0).getPartido(),
                       pronosObtenidos.get(1).getPartido(),
                       pronosObtenidos.get(0).getPronosticoPais1(),
                       pronosObtenidos.get(0).getPronosticoPais2(),
                       pronosObtenidos.get(1).getPronosticoPais1(),
                       pronosObtenidos.get(1).getPronosticoPais2()};
        Object[] o2 = {pronosticos.get(0).getPartido(),
                       pronosticos.get(1).getPartido(),
                       GOLES_PRON_1,
                       GOLES_PRON_2,
                       GOLES_PRON_3,
                       GOLES_PRON_4};

        assertArrayEquals(o2, o2);
    }

    @Test
    public void testCompareToRamaMenor(){
        int comparacionParticipante = 0;
        if (participante1.getPuntajeTotal() < participante2.getPuntajeTotal()) {
            comparacionParticipante = 1;
        } else if (participante2.getPuntajeTotal()
                < participante1.getPuntajeTotal()) {
            comparacionParticipante = -1;
        }

        assertEquals(comparacionParticipante,
                     participante1.compareTo(participante2));
    }
    
    @Test
    public void testCompareToRamaMayor(){
        int comparacionParticipante = 0;
        participante1.setPuntajeTotal(PUNTAJE_TOTAL_2);
        participante2.setPuntajeTotal(PUNTAJE_TOTAL_1);
        
        if (participante1.getPuntajeTotal() < participante2.getPuntajeTotal()) {
            comparacionParticipante = 1;
        } else if (participante2.getPuntajeTotal()
                < participante1.getPuntajeTotal()) {
            comparacionParticipante = -1;
        }

        assertEquals(comparacionParticipante,
                     participante1.compareTo(participante2));
    }
    
    @Test
    public void testCompareToRamaIgual(){
        int comparacionParticipante = 0;
        participante1.setPuntajeTotal(PUNTAJE_TOTAL_1);
        participante2.setPuntajeTotal(PUNTAJE_TOTAL_1);
        
        if (participante1.getPuntajeTotal() < participante2.getPuntajeTotal()) {
            comparacionParticipante = 1;
        } else if (participante2.getPuntajeTotal()
                < participante1.getPuntajeTotal()) {
            comparacionParticipante = -1;
        }

        assertEquals(comparacionParticipante,
                     participante1.compareTo(participante2));
    }
}
