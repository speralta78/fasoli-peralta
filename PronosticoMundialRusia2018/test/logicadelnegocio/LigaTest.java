/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logicadelnegocio;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.internal.runners.statements.Fail;
import org.junit.rules.ExpectedException;

/**
 *
 * @author speralta
 */

//@RunWith(JUnitParamsRunner.class)
public class LigaTest {
    public static Liga liga;

    public LigaTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        liga = Liga.obtenerInstanciaLiga();
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        liga.eliminarParticipante("Pina");
    }

    @After
    public void tearDown() {

    }

    public ArrayList<Partido> cargaDatosDeFixture(String rutaArchivo) {
        BufferedReader br;
        String linea;
        String separador = ",";

        ArrayList<Partido> partidosDeFixture = new ArrayList<>();
        try {
            br = new BufferedReader(new FileReader(rutaArchivo));

            while ((linea = br.readLine()) != null) {
                String[] datosDePartido = linea.split(separador);
                String grupo      = datosDePartido[5];
                String fase       = datosDePartido[0];
                String pais1      = datosDePartido[3];
                String pais2      = datosDePartido[4];
                String goles1Aux  = datosDePartido[6];
                String goles2Aux  = datosDePartido[7];
                String lugar      = datosDePartido[2];     
                String fechaTexto = datosDePartido[1];
                DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                Date fecha              = format.parse(fechaTexto);

                Integer golesPais1 = null;
                Integer golesPais2 = null;

                if (!goles1Aux.equals("NA")) {
                    golesPais1 = Integer.parseInt(goles1Aux);
                }

                if (!goles2Aux.equals("NA")) {
                    golesPais2 = Integer.parseInt(goles2Aux);
                }

                partidosDeFixture.add(new Partido(grupo, fase,
                        pais1, pais2, golesPais1, golesPais2, lugar, fecha));
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Liga.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | ParseException ex) {
            Logger.getLogger(Liga.class.getName()).log(Level.SEVERE, null, ex);
        }

        return partidosDeFixture;
    }

    /**
     * Test of getPartidos method, of class Liga.
     */
    @Test
    public void testGetSetPartidos() {
        System.out.println("getPartidos");
        String ruta = "DB\\Fixture.csv";
        liga.setArchivoCSV(ruta);
        liga.setPartidos();

        ArrayList<Partido> datosDePartido = cargaDatosDeFixture(ruta);
        ArrayList<Partido> resultadosDeLiga = liga.getPartidos();

        for (int i = 0; i < resultadosDeLiga.size(); i++) {
            Partido partido1 = datosDePartido.get(i);
            Partido partido2 = resultadosDeLiga.get(i);

            Object[] o1 = {partido1.getGrupo(), partido1.getFase(),
                partido1.getPais1(), partido1.getGolesPais1(),
                partido1.getGolesPais2(), partido1.getLugar(),
                partido1.getFechaHora()};
            Object[] o2 = {partido2.getGrupo(), partido2.getFase(),
                partido2.getPais1(), partido2.getGolesPais1(),
                partido2.getGolesPais2(), partido2.getLugar(),
                partido2.getFechaHora()};

            assertArrayEquals(o2, o2);
        }
    }

    /**
     * Test of getParticipantes method, of class Liga.
     */
    @Test
    public void testGetParticipantes() throws Exception {
        System.out.println("getParticipantes");
        Participante participante = new Participante("Pina");
        liga.agregarParticipante(participante);
        assertEquals(participante, liga.getParticipantes().get(0));
    }

    @Test
    public void testNoCSVFile() {
        System.out.println("testNoCSVFile");
        liga.setArchivoCSV("missing.csv");
        liga.setPartidos();
    }

    @Test
    public void testParseException() {
        System.out.println("FixtureParseExeption.csv");
        liga.setArchivoCSV("DB\\FixtureParseExeption.csv");
        liga.setPartidos();
    }

    @Test
    public void testAgregarParticipanteNuevo() {
        System.out.println("testAgregarParticipanteNuevo");
        Participante participante = new Participante("Pina");
        try {
            liga.agregarParticipante(participante);
        } catch (Exception ex) {
            System.err.println("No se pudo agregar al participante");
        }

        Participante participanteAgregado = liga.getParticipantes().get(0);

        assertEquals(participante, participanteAgregado);

    }

    @Test
    public void testAgregarParticipanteExistente() {
        System.out.println("testAgregarParticipanteExistente");

        Participante participante = new Participante("Pina");

        try {
            liga.agregarParticipante(participante);
        } catch (Exception ex) {
            System.err.println("No se pudo agregar al participante");
        }

        try {
            liga.agregarParticipante(participante);
        } catch (Exception ex) {
            assertEquals(true, true);
        }
    }

    @Test
    public void testBorrarParticipanteExistente() {
        System.out.println("testBorrarParticipanteExistente");
        Participante participante = new Participante("Pina");

        try {
            liga.agregarParticipante(participante);
        } catch(Exception ex) {
            System.err.println("No se pudo agregar al participante");
        }

        liga.eliminarParticipante("Pina");

        assertEquals(0, liga.getParticipantes().size());

    }

    @Test
    public void testBorrarParticipanteNoExistente() {
        System.out.println("testBorrarParticipanteNoExistente");
        int index = liga.eliminarParticipante("Pina");

        assertEquals(-1, index);
    }

    @Test
    public void testgetListaTextoParticipantes() throws Exception {
        System.out.println("testgetListaTextoParticipantes");

        Participante participante = new Participante("Pina");

        liga.agregarParticipante(participante);

        assertEquals("Pina", liga.getListaTextoParticipantes().get(0));
    }

    @Test
    public void testModificarParticipanteExistente() throws Exception {
        System.out.println("testModificarParticipanteExistente");

        Participante participante = new Participante("Pina");

        liga.agregarParticipante(participante);
        Participante modificado =
                liga.modificarParticipante("Pina", "Pina Nuevo");

        assertEquals("Pina Nuevo", modificado.getAlias());

        liga.modificarParticipante("Pina Nuevo", "Pina");
    }

    @Test
    public void testModificarParticipanteNoExistente() {
        System.out.println("testModificarParticipanteNoExistente");

        try {
            Participante modificado =
                    liga.modificarParticipante("Pina", "Pina Nuevo");
        } catch (Exception ex) {
            assertEquals(true, true);
        }
    }

    @Test
    public void testModificarParticipanteExistenteSinNombre() {
        System.out.println("testModificarParticipanteExistenteSinNombre");

        Participante participante = new Participante("Pina");

        try {
            liga.agregarParticipante(participante);
        } catch (Exception ex) {
            System.out.println("No se pudo agregar al participante");
        }

        try {
            liga.modificarParticipante("Pina", "");
        } catch (Exception ex) {
            assertEquals(true, true);
        }
    }

    @Test
    public void testCalcularRanking() throws Exception {
        System.out.println("testCalcularRanking");
        
        ReglasDeJuego reglas = ReglasDeJuego.obtenerInstanciaReglasDeJuego();
        reglas.setResultadoExactoFaseGrupo(10);
        liga.setPartidos();
        Participante participante = new Participante("Pina");
        liga.agregarParticipante(participante);
        participante.getPronosticos().get(0).setPronosticoPais1(5);
        participante.getPronosticos().get(0).setPronosticoPais2(0);
        liga.calcularRanking();
        Object o1 = 10;
        Object o2 = participante.getPuntajeTotal();
        assertEquals(o1, o2);
    }
    
    @Test
    public void testBorrarDatosLiga() {
        System.out.println("testBorrarDatosLiga");
        liga.borrarDatosLiga();
        Object[] expecteds = {0, 0};
        Object[] actuals = {liga.getParticipantes().size(),
            liga.getPartidos().size()};
        assertArrayEquals(expecteds, actuals);
    }
}
