/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pronosticomundialrusia2018;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import logicadelnegocio.Liga;
import logicadelnegocio.Participante;
import logicadelnegocio.Partido;
import logicadelnegocio.Pronostico;
import java.util.Date;

/**
 *
 * @author USER
 */
public class PronosticarPartidos extends javax.swing.JFrame {
    private Liga liga = Liga.obtenerInstanciaLiga();
    private ArrayList<Participante> participantes = liga.getParticipantes();
    private ArrayList<Partido> partidos = liga.getPartidos();
    private ArrayList<String> fases = new ArrayList<>();
    private Participante participante = null;
    
    /**
     * Creates new form PronosticarPartidos
     */
    public PronosticarPartidos() {
        initComponents();
        cargarListaParticipantes();
        cargarListaFases();
    }
    
    private void cargarListaParticipantes() {
        for(Participante participante : participantes){
            jComboBoxParticipantes.addItem(participante.getAlias());            
        }       
    }
    
    private void cargarListaFases() {
        for(Partido partido: partidos){
            if (!fases.contains(partido.getFase())) {
                fases.add(partido.getFase());
                jComboBoxFases.addItem(partido.getFase());
            }
        }
    }

    private void cargarPartidosParticipante(){
        DefaultTableModel dtmPartidos = (DefaultTableModel) tblPartidos.getModel();
        dtmPartidos.setRowCount(0);
            
        List<Pronostico> pronosticos = participante.getPronosticos();
        for (Pronostico pronostico : pronosticos) {
            if(jComboBoxFases.getSelectedItem().equals(pronostico.getPartido().getFase())){
                String pais1 = pronostico.getPartido().getPais1();
                String pais2 = pronostico.getPartido().getPais2();
                
                Integer resultadoPais1 = pronostico.getPronosticoPais1();
                Integer resultadoPais2 = pronostico.getPronosticoPais2();
                String resultadoPaisTxt1 = "";
                String resultadoPaisTxt2 = "";
                
                if(resultadoPais1 != null){
                    resultadoPaisTxt1 = resultadoPais1.toString();
                }
                
                if(resultadoPais2 != null){
                    resultadoPaisTxt2 = resultadoPais2.toString();
                }
                
                String lugar = pronostico.getPartido().getLugar();
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                String fechaHora = dateFormat.format(pronostico.getPartido().getFechaHora());
                String grupo = pronostico.getPartido().getGrupo();
                String fase = pronostico.getPartido().getFase();
                
                String[] fila = {pais1, pais2, resultadoPaisTxt1, resultadoPaisTxt2, lugar, fechaHora, grupo};
                dtmPartidos.addRow(fila);
            }
        }
    }
    
    private void guardarPartidosParticipante(){
        DefaultTableModel dtmPartidos = (DefaultTableModel) tblPartidos.getModel();
        
        
        if (!dtmPartidos.getValueAt(0, 0).toString().equals("A determinar")){
            for (int i = 0; i < dtmPartidos.getRowCount(); i++){
                
                if (dtmPartidos.getValueAt(i, 5).toString().length() > 0) {
                    
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                    String stringFechaHoraPartido = dtmPartidos.getValueAt(i, 5).toString();
                    try {
                        Date fechaHoraPartido = dateFormat.parse(stringFechaHoraPartido);    
                        Date fechaHoraActual = new Date();
                        if (fechaHoraPartido.after(fechaHoraActual)) {
                            Integer pronosticoPais1 = null;
                            Integer pronosticoPais2 = null;

                            if (dtmPartidos.getValueAt(i, 2).toString().length() > 0) {
                                pronosticoPais1 = Integer.valueOf(dtmPartidos.getValueAt(i, 2).toString());
                            }

                            if (dtmPartidos.getValueAt(i, 3).toString().length() > 0) {
                                pronosticoPais2 = Integer.valueOf(dtmPartidos.getValueAt(i, 3).toString());
                            }

                            int j = 0;
                            boolean validacion1, validacion2, validacion3, validacion4;

                            do {
                                validacion1 = participante.getPronosticos().get(j).getPartido().getPais1().equals(dtmPartidos.getValueAt(i, 0).toString());
                                validacion2 = participante.getPronosticos().get(j).getPartido().getPais2().equals(dtmPartidos.getValueAt(i, 1).toString());
                                validacion3 = participante.getPronosticos().get(j).getPartido().getGrupo().equals(dtmPartidos.getValueAt(i, 6).toString());
                                validacion4 = participante.getPronosticos().get(j).getPartido().getFase().equals(jComboBoxFases.getSelectedItem().toString());
                                j++;
                            }while (!(validacion1 && validacion2 && validacion3 && validacion4));            

                            j--;

                            participante.getPronosticos().get(j).setPronosticoPais1(pronosticoPais1);
                            participante.getPronosticos().get(j).setPronosticoPais2(pronosticoPais2);                            
                        }
                    }catch (ParseException e){
                    }
                    
                }

            }
            JOptionPane.showMessageDialog(this, "Los pronósticos se han guardado correctamente.");
        } else {
            JOptionPane.showMessageDialog(this, "Los países aún no están definidos. No puede guardarse el pronóstico.");
            for (int i = 0; i < dtmPartidos.getRowCount(); i++) {
                dtmPartidos.setValueAt(null, i, 2);
                dtmPartidos.setValueAt(null, i, 3);
            }            
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblPartidos = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jComboBoxParticipantes = new javax.swing.JComboBox<>();
        jComboBoxFases = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        jBotonAceptar = new javax.swing.JButton();
        lblFechaHora = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setText("Pronosticar Partidos");

        tblPartidos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Pais1", "Pais2", "Goles País1", "Goles País2", "Lugar", "Fecha-Hora", "Grupo"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, true, true, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblPartidos);

        jButton1.setText("Guardar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Cancelar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Participante");

        jComboBoxParticipantes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] {"Seleccionar participante"}));
        jComboBoxParticipantes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxParticipantesActionPerformed(evt);
            }
        });

        jComboBoxFases.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] {"Seleccionar fase"}));

        jLabel3.setText("Todas las horas son locales");

        jBotonAceptar.setText("Buscar");
        jBotonAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBotonAceptarActionPerformed(evt);
            }
        });

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date date = new Date();
        lblFechaHora.setText(dateFormat.format(date));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(lblFechaHora))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addGap(190, 190, 190)
                            .addComponent(jButton1)
                            .addGap(132, 132, 132)
                            .addComponent(jButton2)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addComponent(jLabel2)
                            .addGap(18, 18, 18)
                            .addComponent(jComboBoxParticipantes, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jComboBoxFases, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(jBotonAceptar))
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 699, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(60, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(lblFechaHora))
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jComboBoxParticipantes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBoxFases, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBotonAceptar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 418, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton1)
                        .addComponent(jButton2))
                    .addComponent(jLabel3))
                .addContainerGap(40, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        this.setVisible(false);
        new MenuPrincipal().setVisible(true);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jComboBoxParticipantesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxParticipantesActionPerformed
        
    }//GEN-LAST:event_jComboBoxParticipantesActionPerformed

    private void jBotonAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBotonAceptarActionPerformed
        if(jComboBoxParticipantes.getSelectedItem().equals("Seleccionar participante")){
            JOptionPane.showMessageDialog(this, "Se debe seleccionar un participante");
        } else if(jComboBoxFases.getSelectedItem().equals("Seleccionar fase")){
            JOptionPane.showMessageDialog(this, "Se debe seleccionar una fase");
        } else {
            int contador = 0;
            do{
                participante = participantes.get(contador);
                contador++;
            }while (!jComboBoxParticipantes.getSelectedItem().equals(participante.getAlias()));
                
            cargarPartidosParticipante();
        }
    }//GEN-LAST:event_jBotonAceptarActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        guardarPartidosParticipante();
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PronosticarPartidos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PronosticarPartidos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PronosticarPartidos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PronosticarPartidos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PronosticarPartidos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBotonAceptar;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JComboBox<String> jComboBoxFases;
    private javax.swing.JComboBox<String> jComboBoxParticipantes;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblFechaHora;
    private javax.swing.JTable tblPartidos;
    // End of variables declaration//GEN-END:variables
}
