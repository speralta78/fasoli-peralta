/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pronosticomundialrusia2018;


import java.util.Random;
import logicadelnegocio.Liga;
import logicadelnegocio.Participante;
import logicadelnegocio.Pronostico;
import logicadelnegocio.ReglasDeJuego;

/**
 *
 * @author speralta
 */
public class DatosPrueba {
   
    
    public DatosPrueba() {
        Liga liga = Liga.obtenerInstanciaLiga();
        liga.borrarDatosLiga();
        liga.setPartidos();
        try{
            Participante participante1 = new Participante("Seba");
            liga.agregarParticipante(participante1);            
            
            Participante participante2 = new Participante("Pina");
            liga.agregarParticipante(participante2);

            Participante participante3 = new Participante("Pepe");
            liga.agregarParticipante(participante3);
            
            Participante participante4 = new Participante("Ceci");
            liga.agregarParticipante(participante4);
            
            ReglasDeJuego rdj = ReglasDeJuego.obtenerInstanciaReglasDeJuego();
        
            rdj.setAciertoGanadorFaseGrupo(3);
            rdj.setAciertoEmpateFaseGrupo(3);
            rdj.setResultadoExactoFaseGrupo(5);
            rdj.setAciertoGanadorFaseFinal(4);
            rdj.setAciertoEmpateFaseFinal(4);
            rdj.setResultadoExactoFaseFinal(7);
            
            Random randomGenerator = new Random();
            for (Pronostico pronostico : participante1.getPronosticos()) {
                int randomInt = randomGenerator.nextInt(6);
                pronostico.setPronosticoPais1(randomInt);
                randomInt = randomGenerator.nextInt(6);
                pronostico.setPronosticoPais2(randomInt);
            }
            
            for (Pronostico pronostico : participante2.getPronosticos()) {
                int randomInt = randomGenerator.nextInt(6);
                pronostico.setPronosticoPais1(randomInt);
                randomInt = randomGenerator.nextInt(6);
                pronostico.setPronosticoPais2(randomInt);
            }
            
            for (Pronostico pronostico : participante3.getPronosticos()) {
                int randomInt = randomGenerator.nextInt(6);
                pronostico.setPronosticoPais1(randomInt);
                randomInt = randomGenerator.nextInt(6);
                pronostico.setPronosticoPais2(randomInt);
            }
            
            for (Pronostico pronostico : participante4.getPronosticos()) {
                int randomInt = randomGenerator.nextInt(6);
                pronostico.setPronosticoPais1(randomInt);
                randomInt = randomGenerator.nextInt(6);
                pronostico.setPronosticoPais2(randomInt);
            }            
        }
        catch(Exception ex){
 
            System.out.println(ex.getMessage());
 
        }
    }
}
