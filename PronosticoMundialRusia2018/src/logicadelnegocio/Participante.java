/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package logicadelnegocio;

import java.util.ArrayList;

/**
 *
 * @author speralta
 */
public class Participante implements Comparable<Participante>{

    private String alias;
    private Integer puntajeTotal;
    private ArrayList<Pronostico> pronosticos = new ArrayList<>();

    public Participante(String alias) {
        this.alias = alias;
        Liga l = Liga.obtenerInstanciaLiga();
        ArrayList<Partido> partidos = l.getPartidos();
        for (Partido partido : partidos) {
            this.pronosticos.add(new Pronostico(partido));
        }
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getAlias() {
        return this.alias;
    }

    public void setPuntajeTotal(int puntajeTotal) {
        this.puntajeTotal = puntajeTotal;
    }

    public Integer getPuntajeTotal() {
        return this.puntajeTotal;
    }

    public void setPronosticos(ArrayList<Pronostico> pronosticos) {
        this.pronosticos = pronosticos;
    }

    public ArrayList<Pronostico> getPronosticos() {
        return this.pronosticos;
    }

    @Override
    public int compareTo(Participante participanteComparacion) {
        if (this.puntajeTotal < participanteComparacion.puntajeTotal) {
            return 1;
        }
        if (this.puntajeTotal > participanteComparacion.puntajeTotal) {
            return -1;
        } else {
            return 0;
        }
    }
}
