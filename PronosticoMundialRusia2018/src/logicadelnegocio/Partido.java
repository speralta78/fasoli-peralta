/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package logicadelnegocio;

import java.util.Date;

/**
 *
 * @author speralta
 */
public class Partido {
    private String grupo;
    private String fase;
    private String pais1;
    private String pais2;
    private Integer golesPais1;
    private Integer golesPais2;
    private String lugar;
    private Date fechaHora;
    
    public Partido() {

    }
    
    public Partido(String grupo, String fase, String pais1, String pais2,
            Integer golesPais1, Integer golesPais2, String lugar, Date fechaHora) {
        this.grupo      = grupo;
        this.fase       = fase;
        this.pais1      = pais1;
        this.pais2      = pais2;
        this.golesPais1 = golesPais1;
        this.golesPais2 = golesPais2;
        this.lugar      = lugar;
        this.fechaHora  = fechaHora;
    }

    public boolean equals(Partido partido){
        return (this.grupo.equals(partido.getGrupo()))
               && (this.fase.equals(partido.getFase()))
               && (this.pais1.equals(partido.getPais1()))
               && (this.pais2.equals(partido.getPais2()))
               && (this.golesPais1.equals(partido.getGolesPais1()))
               && (this.golesPais2.equals(partido.getGolesPais2()))
               && (this.lugar.equals(partido.getLugar()))
               && (this.fechaHora.equals(partido.getFechaHora()));
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public void setFase(String fase) {
        this.fase = fase;
    }

    public void setPais1(String pais1) {
        this.pais1 = pais1;
    }

    public void setPais2(String pais2) {
        this.pais2 = pais2;
    }

    public void setGolesPais1(Integer golesPais1) {
        this.golesPais1 = golesPais1;
    }

    public void setGolesPais2(Integer golesPais2) {
        this.golesPais2 = golesPais2;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    public String getGrupo() {
        return grupo;
    }

    public String getFase() {
        return fase;
    }

    public String getPais1() {
        return pais1;
    }

    public String getPais2() {
        return pais2;
    }

    public Integer getGolesPais1() {
        return golesPais1;
    }

    public Integer getGolesPais2() {
        return golesPais2;
    }

    public String getLugar() {
        return lugar;
    }

    public Date getFechaHora() {
        return fechaHora;
    }
}
