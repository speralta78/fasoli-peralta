/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package logicadelnegocio;

/**
 *
 * @author speralta
 */
public class ReglasDeJuego {
    private static ReglasDeJuego instanciaReglasDeJuego;
    private int aciertoGanadorFaseFinal;
    private int aciertoEmpateFaseGrupo;
    private int aciertoEmpateFaseFinal;
    private int resultadoExactoFaseGrupo;
    private int resultadoExactoFaseFinal;

    private int aciertoGanadorFaseGrupo;

    private ReglasDeJuego() {

    }
    
    public static ReglasDeJuego obtenerInstanciaReglasDeJuego() {
        if (instanciaReglasDeJuego == null) {
            instanciaReglasDeJuego = new ReglasDeJuego();
        }
        return instanciaReglasDeJuego;        
    }

    public void setAciertoGanadorFaseGrupo(int aciertoGanadorFaseGrupo) {
        this.aciertoGanadorFaseGrupo = aciertoGanadorFaseGrupo;
    }

    public void setAciertoGanadorFaseFinal(int aciertoGanadorFaseFinal) {
        this.aciertoGanadorFaseFinal = aciertoGanadorFaseFinal;
    }

    public void setAciertoEmpateFaseGrupo(int aciertoEmpateFaseGrupo) {
        this.aciertoEmpateFaseGrupo = aciertoEmpateFaseGrupo;
    }

    public void setAciertoEmpateFaseFinal(int aciertoEmpateFaseFinal) {
        this.aciertoEmpateFaseFinal = aciertoEmpateFaseFinal;
    }

    public void setResultadoExactoFaseGrupo(int resultadoExactoFaseGrupo) {
        this.resultadoExactoFaseGrupo = resultadoExactoFaseGrupo;
    }

    public void setResultadoExactoFaseFinal(int resultadoExactoFaseFinal) {
        this.resultadoExactoFaseFinal = resultadoExactoFaseFinal;
    }

    public int getAciertoGanadorFaseGrupo() {
        return aciertoGanadorFaseGrupo;
    }

    public int getAciertoGanadorFaseFinal() {
        return aciertoGanadorFaseFinal;
    }

    public int getAciertoEmpateFaseGrupo() {
        return aciertoEmpateFaseGrupo;
    }

    public int getAciertoEmpateFaseFinal() {
        return aciertoEmpateFaseFinal;
    }

    public int getResultadoExactoFaseGrupo() {
        return resultadoExactoFaseGrupo;
    }

    public int getResultadoExactoFaseFinal() {
        return resultadoExactoFaseFinal;
    }
}
