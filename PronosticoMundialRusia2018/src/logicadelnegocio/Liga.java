/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package logicadelnegocio;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

/**
 *
 * @author speralta
 */

public class Liga {
    private String archivoCsv = "DB\\Fixture.csv";
    private static Liga instanciaLiga = null;
    private ArrayList<Partido> partidos;
    private ArrayList<Participante> participantes = new ArrayList<>();

    private Liga() {
        this.setPartidos();
    }

    public static Liga obtenerInstanciaLiga() {
        if (instanciaLiga == null) {
            instanciaLiga = new Liga();
        }
        return instanciaLiga;
    }

    public ArrayList<Partido> getPartidos() {
        return partidos;
    }

    public void setPartidos() {
        this.partidos = importarFixture();
    }

    public void setArchivoCSV(String path){
        this.archivoCsv = path;
    }

    public void agregarParticipante(Participante participante) throws Exception {
        //Chequeo que no exista un participante duplicado
        Iterator<Participante> it = participantes.iterator();
        boolean hayRepetido = false;
        while (it.hasNext() && !hayRepetido) {
            Participante participantAux = it.next();
            if (participantAux.getAlias().equals(participante.getAlias())) {
                hayRepetido = true;
            }
        }

        if (!hayRepetido) {
            participantes.add(participante);
        } else {
            throw new Exception("Ya existe un participante"
                    + "con el alias ingresado");
        }
    }

     public Participante modificarParticipante(String oldAlias,
             String newAlias) throws Exception{
        if (!newAlias.isEmpty()) {
            //Modifico un participante ya creado
            Iterator<Participante> it = participantes.iterator();
            boolean encontro = false;
            Participante participante = null;

            while (it.hasNext() && !encontro) {
                participante = it.next();
                if (participante.getAlias().equals(oldAlias)) {
                    participante.setAlias(newAlias);
                    encontro = true;
                }
            }

            if (encontro) {
                return participante;
            } else {
                throw new Exception("Debe ingresar"
                        + "el participante a modificar");
            }
        } else {
            throw new Exception("El nuevo alias"
                    + "del participante no puede ser vacio");
        }

    }

    public int eliminarParticipante(String alias){
        //Borro el participante con cuyo alias coincide con alias y retorna su
        //indice para borrarlo del modelo de la lista
        int index = 0;
        boolean encontro = false;
        Iterator<Participante> it = participantes.iterator();

        while (it.hasNext() && !encontro) {
            index++;
            Participante participante = it.next();
            if (participante.getAlias().equals(alias)) {
                participantes.remove(participante);
                encontro = true;
            }
        }

        if (encontro) {
            return index;
        } else {
            return -1;
        }
    }

    public ArrayList<Participante> getParticipantes() {
        return this.participantes;
    }

    public ArrayList<String> getListaTextoParticipantes() {
        ArrayList<String> listaTextoParticipantes = new ArrayList();

        for (Participante participante : this.participantes) {
            listaTextoParticipantes.add(participante.getAlias());
        }

        return listaTextoParticipantes;
    }

    private ArrayList<Partido> importarFixture() {
        BufferedReader br;
        String linea;
        String separador = ",";

        ArrayList<Partido> partidosDeFixture = new ArrayList<>();
        try {
            br = new BufferedReader(new FileReader(this.archivoCsv));

            while ((linea = br.readLine()) != null) {
                String[] datosDePartido = linea.split(separador);

                if (datosDePartido.length == 8) {
                    String grupo            = datosDePartido[5];
                    String fase             = datosDePartido[0];
                    String pais1            = datosDePartido[3];
                    String pais2            = datosDePartido[4];
                    String goles1Aux        = datosDePartido[6];
                    String goles2Aux        = datosDePartido[7];
                    String lugar            = datosDePartido[2];

                    String fechaTexto       = datosDePartido[1];
                    DateFormat format;
                    format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                    Date fecha              = format.parse(fechaTexto);

                    Integer golesPais1 = null;
                    Integer golesPais2 = null;

                    if (!goles1Aux.equals("NA")) {
                        golesPais1 = Integer.parseInt(goles1Aux);
                    }

                    if (!goles2Aux.equals("NA")) {
                        golesPais2 = Integer.parseInt(goles2Aux);
                    }

                    partidosDeFixture.add(new Partido(grupo, fase,
                            pais1, pais2, golesPais1, golesPais2,
                            lugar, fecha));
                }
            }
        } catch (FileNotFoundException ex) {
            System.err.println("No se pudo cargar el fixture. "
                    + "No se encontró el archivo: " + archivoCsv + ".");
        } catch (IOException | ParseException ex) {
            System.err.println("No se pudo cargar el fixture. "
                    + "No se pudo parsear la fecha.");
        }
        return partidosDeFixture;
    }

    public void calcularRanking() {        

        for (Participante participante : this.participantes) {
            Integer totalPuntosParticipante = 0;
            participante.setPuntajeTotal(0);
            for (Pronostico pronostico : participante.getPronosticos()) {
                totalPuntosParticipante += pronostico.getPuntaje();
            }
            participante.setPuntajeTotal(totalPuntosParticipante);
        }
    }

    public void borrarDatosLiga() {
        this.partidos.clear();
        this.participantes.clear();
    }
}
