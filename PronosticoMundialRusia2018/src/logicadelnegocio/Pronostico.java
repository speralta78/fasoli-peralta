/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package logicadelnegocio;

/**
 *
 * @author speralta
 */
public class Pronostico {
    private static final String FASE_GRUPO = "GRUPO";
    
    public ReglasDeJuego reglasDeJuego;
    public Partido partido;
    private Integer pronosticoPais1;
    private Integer pronosticoPais2;

    public Pronostico(Partido partido) {
        this.partido = partido;
        this.pronosticoPais1 = null;
        this.pronosticoPais2 = null;
        this.reglasDeJuego = ReglasDeJuego.obtenerInstanciaReglasDeJuego();

    }

    public void setPronosticoPais1(Integer pronosticoPais1) {
        this.pronosticoPais1 = pronosticoPais1;
    }

    public void setPronosticoPais2(Integer pronosticoPais2) {
        this.pronosticoPais2 = pronosticoPais2;
    }

    public Integer getPronosticoPais1() {
        return pronosticoPais1;
    }

    public Integer getPronosticoPais2() {
        return pronosticoPais2;
    }

    public Partido getPartido() {
        return partido;
    }

    public Integer getPuntaje() {
        return this.calcularPuntaje();
    }

    private Integer calcularPuntaje() {
        Integer puntajeObtenido = 0;

        if (this.partido.getGolesPais1() != null
                && this.partido.getGolesPais2() != null
                && this.pronosticoPais1 != null
                && this.pronosticoPais2 != null) {

            Integer puntosAciertoEmpate = 0;
            Integer puntosAciertoGanador = 0;
            Integer puntosAciertoExacto = 0;


            if (this.partido.getFase().equals(FASE_GRUPO)) {
                puntosAciertoEmpate = this.reglasDeJuego.getAciertoEmpateFaseGrupo();
                puntosAciertoGanador = this.reglasDeJuego.getAciertoGanadorFaseGrupo();
                puntosAciertoExacto = this.reglasDeJuego.getResultadoExactoFaseGrupo();
            } else {
                puntosAciertoEmpate = this.reglasDeJuego.getAciertoEmpateFaseFinal();
                puntosAciertoGanador = this.reglasDeJuego.getAciertoGanadorFaseFinal();
                puntosAciertoExacto = this.reglasDeJuego.getResultadoExactoFaseFinal();
            }



            if (this.partido.getGolesPais1() == this.pronosticoPais1
                    && this.partido.getGolesPais2() == this.pronosticoPais2) {

                puntajeObtenido = puntosAciertoExacto;

            } else if (this.partido.getGolesPais1() == this.partido.getGolesPais2() 
                    && this.pronosticoPais1 == this.pronosticoPais2) {

                puntajeObtenido = puntosAciertoEmpate;

            } else if ((this.partido.getGolesPais1() > this.partido.getGolesPais2()
                    && this.pronosticoPais1 > this.pronosticoPais2)
                    || (this.partido.getGolesPais1() < this.partido.getGolesPais2()
                    && this.pronosticoPais1 < this.pronosticoPais2)) {

                puntajeObtenido = puntosAciertoGanador;
            } else {
                puntajeObtenido = 0;
            }
        }

        return puntajeObtenido;
    }
}
